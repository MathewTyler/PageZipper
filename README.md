PageZipper
==========

PageZipper is a free bookmarklet which automatically merges all the 'Next' pages into one page
> Ever read one of those top ten lists or photo galleries where every item is on a different page? 
> Clicking "Next" and waiting takes longer than actually reading the page. 
> PageZipper automatically merges all the "Next" pages into one, so you can skip directly to the stuff you want. 

PageZipper is distributed as a:

* Bookmarklet - www.printwhatyoulike.com/pagezipper
* Chrome Extension - https://chrome.google.com/webstore/detail/pagezipper/fbbmnbomimdgmecfpbilhoafgmmeagef?hl=en
* Firefox Extension - https://addons.mozilla.org/en-us/firefox/addon/pagezipper/

PageZipper is open source. Contributions are welcome